#include <iostream>
#include <memory>
#include <functional>

#define TRACE_DESTRUCTION

class LifetimeMonitor {
    friend class Lifetime;
public:
    bool expired() const {
        return m_weakLifeHolder.expired();
    }
    
protected:
    LifetimeMonitor(std::shared_ptr<bool> LifeHolder) : m_weakLifeHolder(LifeHolder) {}
    
private:
    std::weak_ptr<bool> m_weakLifeHolder;
};

class Lifetime {
public:
    Lifetime() = default;
    
#ifdef TRACE_DESTRUCTION
    ~Lifetime(){
        std::cout << "Lifetime ended" << std::endl;
    }
#endif

    LifetimeMonitor getLifetime() {
        return {m_LifeHolder};
    }

private:
    std::shared_ptr<bool> m_LifeHolder = std::make_shared<bool>(true);
};

// Test below

struct LambdaCaller {
    void registerLambda(std::function<void()> func){
        m_func = func;
    }
    
    void call(){
        if (!m_func){
            std::cout << " Test error " << std::endl;
            return;
        }
        m_func();
    }
    
    std::function<void()> m_func;
};

// Example usage. Either inherit from Lifetime or have it as a member

struct LambdaUser : public Lifetime {
    void createLambda(LambdaCaller& lc){
        lc.registerLambda([lifetime = getLifetime(), this](){
            std::cout << "lambda called" << std::endl;
            if (lifetime.expired()) {
                std::cout << ">> 'this' is no longer valid in lambda!" << std::endl;
                return;
            }

            std::cout << "Calling this->member-function in lambda" << std::endl;
            member_func();
        });
    }
  
    void member_func(){
        std::cout << "this->member-function called" << std::endl;
    }
    
    ~LambdaUser() {
        std::cout << "Class is getting destroyed." << std::endl;
    }
    
   // Lifetime m_lifetime;
};

int main() {

    {
        Lifetime l1;
        LifetimeMonitor m1 = l1.getLifetime();
        std::cout << std::boolalpha << "m1 expired: " << m1.expired() << " (should be false)" << std::endl;
    }
    
    auto l2 = std::make_unique<Lifetime>();
    LifetimeMonitor m2 = l2->getLifetime();
    std::cout << std::boolalpha << "m2 expired: " << m2.expired() << " (should be false)" << std::endl;
    
    l2.reset();
    std::cout << std::boolalpha << "m2 expired: " << m2.expired() << " (should be true)" << std::endl << std::endl;
    
    
    auto lu = std::make_shared<LambdaUser>();
    auto lc = std::make_shared<LambdaCaller>();
    
    lu->createLambda(*lc);
    lc->call();
    
    lu.reset();
    lc->call(); //would result in undefined behaviour in subsequent lambda call without lifetime check
    return 0;
}
/*
Example output:

m1 expired: false (should be false)
Lifetime ended
m2 expired: false (should be false)
Lifetime ended
m2 expired: true (should be true)

lambda called
Calling this->member-function in lambda
this->member-function called
Class is getting destroyed.
Lifetime ended
lambda called
>> 'this' is no longer valid in lambda!

*/