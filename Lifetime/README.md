# Lifetime

A simple solution to check if "this" is still valid in a lambda, by using a lifetime.expired() function.
Function will return false if "this" was destroyed. Internally, shared- and weak-pointers are utilised.
Intended for use in applications using boost::asio. Mechanism is not threadsafe.