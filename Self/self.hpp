#include <cassert>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <optional>
#include <utility>

template <typename InstanceT>
class Self;

// Uses CRTP https://en.cppreference.com/w/cpp/language/crtp
template <typename InstanceT>
class enable_self_from_this {  // Must NOT be used as a member.
    friend class Self<InstanceT>;
   public:
    ~enable_self_from_this();  // Not virtual

    Self<InstanceT> self_from_this();

   protected:
    enable_self_from_this();
    enable_self_from_this(enable_self_from_this const& o) {}  // Copy does nothing
    enable_self_from_this(enable_self_from_this&& o) = delete;  // No move allowed. Use move_self

    void move_self(InstanceT& oldInstance);

   private:
    std::optional<std::reference_wrapper<InstanceT>> m_instance;
    std::list<Self<InstanceT>*> m_references;  // Pointers onto stack!

    void UpdateAllRefs(bool gettingDestroyed);

    // Called by Self
    void Register(Self<InstanceT>& ref);
    void Deregister(Self<InstanceT>& ref);
};

template <typename InstanceT>
class Self {
    friend class enable_self_from_this<InstanceT>;

   public:
    Self() = delete;
    Self(Self<InstanceT> const& o);
    Self(Self<InstanceT>&& o);
    ~Self();

    InstanceT& get() const;
    InstanceT* operator->() const;
    explicit operator bool() const;

    // For debugging
    void SetOnRefUpdates(
        std::function<void(Self<InstanceT>& self,
                           std::optional<std::reference_wrapper<enable_self_from_this<InstanceT>>> new_enabled_self_from_this)> cb) const {
        m_onUpdate = cb;
    }

   protected:
    Self(enable_self_from_this<InstanceT>& enable_self_from_this);

    void UpdateInstance(
        std::optional<std::reference_wrapper<enable_self_from_this<InstanceT>>>
            enable_self_from_this);

   private:
    std::optional<std::reference_wrapper<enable_self_from_this<InstanceT>>>
        m_self;  // Valid as long as enable_self_from_this exists.

    mutable std::function<void(
        Self<InstanceT>& self,
        std::optional<std::reference_wrapper<enable_self_from_this<InstanceT>>>
            new_enabled_self_from_this)>
        m_onUpdate;
};

// --------------- Implementation of enable_self_from_this below -------------------------
template <typename InstanceT>
enable_self_from_this<InstanceT>::enable_self_from_this()
    : m_instance(static_cast<InstanceT&>(*this)) {
} 

template <typename InstanceT>
enable_self_from_this<InstanceT>::~enable_self_from_this() {
    UpdateAllRefs(true);
}

template <typename InstanceT>
void enable_self_from_this<InstanceT>::move_self(InstanceT& oldInstance) {
    assert(std::addressof(*this) != std::addressof(oldInstance));

    m_references = std::move(oldInstance.m_references);

    // Cleanup old_enable_self_from_this
    oldInstance.m_references.clear();
    oldInstance.m_instance.reset();

    // Notify all refs about the move
    UpdateAllRefs(false);
}

template <typename InstanceT>
void enable_self_from_this<InstanceT>::UpdateAllRefs(bool gettingDestroyed) {
    for (Self<InstanceT>* ref : m_references) {
        ref->UpdateInstance(
            gettingDestroyed ? std::nullopt
                             : std::optional<std::reference_wrapper<enable_self_from_this>>{*this});
    }
}

template <typename InstanceT>
Self<InstanceT> enable_self_from_this<InstanceT>::self_from_this() {
    return {*this};
}

template <typename InstanceT>
void enable_self_from_this<InstanceT>::Register(Self<InstanceT>& ref) {
    m_references.emplace_back(std::addressof(ref));  // Emplace the ref as a ptr
}
template <typename InstanceT>
void enable_self_from_this<InstanceT>::Deregister(Self<InstanceT>& ref) {
    m_references.remove(std::addressof(ref));
}

// -------------- Implementation of Self below -----------------
template <typename InstanceT>
Self<InstanceT>::Self(enable_self_from_this<InstanceT>& enable_self_from_this)
    : m_self(enable_self_from_this) {
    assert(m_self.has_value());
    m_self->get().Register(*this);
}

template <typename InstanceT>
Self<InstanceT>::Self(Self<InstanceT> const& o) : m_self(o.m_self) {
    assert(m_self.has_value());
    m_self->get().Register(*this);
}

template <typename InstanceT>
Self<InstanceT>::Self(Self<InstanceT>&& o) : m_self(o.m_self /*no move!*/) {
    assert(m_self.has_value());
    m_self->get().Register(*this);
}

template <typename InstanceT>
Self<InstanceT>::~Self() {
    if (m_self) {
        m_self->get().Deregister(*this);
    }
}

template <typename InstanceT>
InstanceT& Self<InstanceT>::get() const {
    if (!m_self.has_value()) {
        throw std::runtime_error("self is empty");
    }
    return m_self->get().m_instance->get();
}

template <typename InstanceT>
Self<InstanceT>::operator bool() const {
    return m_self.has_value();
}

template <typename InstanceT>
InstanceT* Self<InstanceT>::operator->() const {
    return &get();
}

template <typename InstanceT>
void Self<InstanceT>::UpdateInstance(
    std::optional<std::reference_wrapper<enable_self_from_this<InstanceT>>>
        new_enabled_self_from_this) {
    // enable_self_from_this was moved or is getting destroyed
    if (m_onUpdate) {
        m_onUpdate(*this, new_enabled_self_from_this);
    }
    assert(m_self);
    m_self = new_enabled_self_from_this;
}

// Callback that can be used for debugging
template <typename InstanceT>
auto onUpdate = [](Self<InstanceT>& self,
       std::optional<std::reference_wrapper<enable_self_from_this<InstanceT>>> new_enabled_self_from_this) {
        if (new_enabled_self_from_this) {
            std::cout << std::addressof(self)
                      << " Self: was updated. New enable_self_from_this: "
                      << &(new_enabled_self_from_this->get()) << std::endl;
        } else {
            std::cout
                << std::addressof(self)
                << " Self: was updated. New enable_self_from_this: EMPTY\n";
        }
    };

// ---------------------- Tests below ---------------------

class TestClass : public enable_self_from_this<TestClass> {
   public:
    TestClass() = default;
    TestClass(TestClass const& o) = default;
    TestClass(TestClass&& o) { move_self(o); }

    auto GetSimpleCallback() {
        return [self = self_from_this()]() {
            self.SetOnRefUpdates(onUpdate<TestClass>);
            if (!self) {
                return false;
            }
            self->MemberFunc();
            self->m_str = "Changed string";
            std::cout << self->m_str << std::endl;
            return true;
        };
    }

    auto GetInstanceGetter() {
        return [self = self_from_this()]() { return self; };
    }

    std::uint64_t GetAddr() { return (std::uint64_t)std::addressof(*this); }

    void MemberFunc() { std::cout << std::addressof(*this) << " bar\n"; }
    std::string m_str = " some very long string that exceeds the sso";
};

int main() {
    std::optional<TestClass> instance{};
    instance.emplace();
    assert(instance.has_value());

    auto instanceGetterCb = instance->GetInstanceGetter();

    auto isSelfInCbValidCb = instance->GetSimpleCallback();
    bool selfValid =
        isSelfInCbValidCb();  // Changes the member string of instance
    assert(instance->m_str == "Changed string");
    assert(selfValid);

    auto self = instance->self_from_this();
    assert(self);
    std::optional movedInstance{std::move(instance.value())};
    assert(movedInstance.has_value());
    assert(self);  // Move must not lead to expiration

    instance.reset();  // Must not have an effect as instance was moved out
    assert(self);

    auto selfStoredInCb = instanceGetterCb();
    assert(movedInstance->GetAddr() == selfStoredInCb->GetAddr());

    movedInstance.reset();
    assert(!self);
    assert(!selfStoredInCb);
    assert(isSelfInCbValidCb() == false);

    std::cout << "OK" << std::endl;
    return 0;
}
